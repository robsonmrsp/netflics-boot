package br.com.netflics.core.rs;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//https://hellokoding.com/spring-boot-hello-world-example-with-jsp/
@Controller
public class HelloController {
	@GetMapping({ "/", "/index" })
	public String index(Model model, @RequestParam(value = "name", required = false, defaultValue = "World") String name) {
		return "index";
	}
}