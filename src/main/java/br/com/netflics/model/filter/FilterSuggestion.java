package br.com.netflics.model.filter;

import java.io.Serializable;

import java.time.LocalDate;
import java.time.LocalDateTime;

/* generated by JSetup v0.95 :  at 24/03/2019 17:51:19 */
public class FilterSuggestion implements Serializable {
	private static final long serialVersionUID = 1L;
	private LocalDate expirationDate;  			

	private Integer movie;		
	private Integer profile;		
	
	public  FilterSuggestion() {
		
	}

	public LocalDate getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDate expirationDate) {
		this.expirationDate = expirationDate;
	}
		
	public Integer getMovie() {
		return movie;
	}
	
	public void setMovie(Integer movie) {
		this.movie = movie;
	}
	public Integer getProfile() {
		return profile;
	}
	
	public void setProfile(Integer profile) {
		this.profile = profile;
	}
}
//generated by JSetup v0.95 :  at 24/03/2019 17:51:19