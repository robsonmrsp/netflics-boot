package br.com.netflics.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import br.com.netflics.model.Movie;
import br.com.netflics.model.filter.FilterMovie;

import org.springframework.data.jpa.domain.Specification;

import br.com.netflics.core.model.Tenant;
@SuppressWarnings("serial")
public class MovieSpecificationHelper {

	public static Specification<Movie> fromId(Integer id, Tenant tenant) {
		return new Specification<Movie>() {
			@Override
			public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				predicates.add(criteriaBuilder.equal(root.get("id"), id));

				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}

	public static Specification<Movie> filter(FilterMovie filterMovie, Tenant tenant) {
		return new Specification<Movie>() {

			@Override
			public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				
				if (filterMovie.getTitle() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("title")), "%" + filterMovie.getTitle().toUpperCase() + "%"));
				}
				if (filterMovie.getPoster() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("poster")), "%" + filterMovie.getPoster().toUpperCase() + "%"));
				}
				if (filterMovie.getDuration() != null) {
					predicates.add(criteriaBuilder.equal(root.get("duration"), filterMovie.getDuration()));
				}				
				if (filterMovie.getReleaseDate() != null) {
					predicates.add(criteriaBuilder.equal(root.get("releaseDate"), filterMovie.getReleaseDate()));
				}				
				if (filterMovie.getSinopse() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("sinopse")), "%" + filterMovie.getSinopse().toUpperCase() + "%"));
				}
				if (filterMovie.getDirector() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("director")), "%" + filterMovie.getDirector().toUpperCase() + "%"));
				}
				if (filterMovie.getMaturityRating() != null) {
					predicates.add(criteriaBuilder.equal(root.get("maturityRating").get("id"), filterMovie.getMaturityRating()));
				}
				if (filterMovie.getGenre() != null) {
					predicates.add(criteriaBuilder.equal(root.get("genre").get("id"), filterMovie.getGenre()));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}
}
