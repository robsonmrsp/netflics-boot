package br.com.netflics.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import br.com.netflics.model.Actor;
import br.com.netflics.model.filter.FilterActor;

import org.springframework.data.jpa.domain.Specification;

import br.com.netflics.core.model.Tenant;
@SuppressWarnings("serial")
public class ActorSpecificationHelper {

	public static Specification<Actor> fromId(Integer id, Tenant tenant) {
		return new Specification<Actor>() {
			@Override
			public Predicate toPredicate(Root<Actor> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				predicates.add(criteriaBuilder.equal(root.get("id"), id));

				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}

	public static Specification<Actor> filter(FilterActor filterActor, Tenant tenant) {
		return new Specification<Actor>() {

			@Override
			public Predicate toPredicate(Root<Actor> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				
				if (filterActor.getBiography() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("biography")), "%" + filterActor.getBiography().toUpperCase() + "%"));
				}
				if (filterActor.getBirthDate() != null) {
					predicates.add(criteriaBuilder.equal(root.get("birthDate"), filterActor.getBirthDate()));
				}				
				if (filterActor.getName() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")), "%" + filterActor.getName().toUpperCase() + "%"));
				}
				if (filterActor.getPicture() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("picture")), "%" + filterActor.getPicture().toUpperCase() + "%"));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}
}
