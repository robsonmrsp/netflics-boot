package br.com.netflics.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import br.com.netflics.model.Suggestion;
import br.com.netflics.model.filter.FilterSuggestion;

import org.springframework.data.jpa.domain.Specification;

import br.com.netflics.core.model.Tenant;
@SuppressWarnings("serial")
public class SuggestionSpecificationHelper {

	public static Specification<Suggestion> fromId(Integer id, Tenant tenant) {
		return new Specification<Suggestion>() {
			@Override
			public Predicate toPredicate(Root<Suggestion> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				predicates.add(criteriaBuilder.equal(root.get("id"), id));

				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}

	public static Specification<Suggestion> filter(FilterSuggestion filterSuggestion, Tenant tenant) {
		return new Specification<Suggestion>() {

			@Override
			public Predicate toPredicate(Root<Suggestion> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				
				if (filterSuggestion.getExpirationDate() != null) {
					predicates.add(criteriaBuilder.equal(root.get("expirationDate"), filterSuggestion.getExpirationDate()));
				}				
				if (filterSuggestion.getMovie() != null) {
					predicates.add(criteriaBuilder.equal(root.get("movie").get("id"), filterSuggestion.getMovie()));
				}
				if (filterSuggestion.getProfile() != null) {
					predicates.add(criteriaBuilder.equal(root.get("profile").get("id"), filterSuggestion.getProfile()));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}
}
