package br.com.netflics.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import br.com.netflics.model.User;
import br.com.netflics.model.filter.FilterUser;

import org.springframework.data.jpa.domain.Specification;

import br.com.netflics.core.model.Tenant;
@SuppressWarnings("serial")
public class UserSpecificationHelper {

	public static Specification<User> fromId(Integer id, Tenant tenant) {
		return new Specification<User>() {
			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				predicates.add(criteriaBuilder.equal(root.get("id"), id));

				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}

	public static Specification<User> filter(FilterUser filterUser, Tenant tenant) {
		return new Specification<User>() {

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				
				if (filterUser.getName() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")), "%" + filterUser.getName().toUpperCase() + "%"));
				}
				if (filterUser.getUsername() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("username")), "%" + filterUser.getUsername().toUpperCase() + "%"));
				}
				if (filterUser.getEmail() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("email")), "%" + filterUser.getEmail().toUpperCase() + "%"));
				}
				if (filterUser.getPassword() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("password")), "%" + filterUser.getPassword().toUpperCase() + "%"));
				}
				if (filterUser.getEnable() != null) {
					predicates.add(criteriaBuilder.equal(root.get("enable"), filterUser.getEnable()));
				}				
				if (filterUser.getImage() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("image")), "%" + filterUser.getImage().toUpperCase() + "%"));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}
}
