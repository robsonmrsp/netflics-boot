package br.com.netflics.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import br.com.netflics.model.Item;
import br.com.netflics.model.filter.FilterItem;

import org.springframework.data.jpa.domain.Specification;

import br.com.netflics.core.model.Tenant;
@SuppressWarnings("serial")
public class ItemSpecificationHelper {

	public static Specification<Item> fromId(Integer id, Tenant tenant) {
		return new Specification<Item>() {
			@Override
			public Predicate toPredicate(Root<Item> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				predicates.add(criteriaBuilder.equal(root.get("id"), id));

				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}

	public static Specification<Item> filter(FilterItem filterItem, Tenant tenant) {
		return new Specification<Item>() {

			@Override
			public Predicate toPredicate(Root<Item> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				
				if (filterItem.getName() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")), "%" + filterItem.getName().toUpperCase() + "%"));
				}
				if (filterItem.getItemType() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("itemType")), "%" + filterItem.getItemType().toUpperCase() + "%"));
				}
				if (filterItem.getIdentifier() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("identifier")), "%" + filterItem.getIdentifier().toUpperCase() + "%"));
				}
				if (filterItem.getDescription() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("description")), "%" + filterItem.getDescription().toUpperCase() + "%"));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}
}
