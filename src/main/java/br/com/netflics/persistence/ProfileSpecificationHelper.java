package br.com.netflics.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import br.com.netflics.model.Profile;
import br.com.netflics.model.filter.FilterProfile;

import org.springframework.data.jpa.domain.Specification;

import br.com.netflics.core.model.Tenant;
@SuppressWarnings("serial")
public class ProfileSpecificationHelper {

	public static Specification<Profile> fromId(Integer id, Tenant tenant) {
		return new Specification<Profile>() {
			@Override
			public Predicate toPredicate(Root<Profile> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				predicates.add(criteriaBuilder.equal(root.get("id"), id));

				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}

	public static Specification<Profile> filter(FilterProfile filterProfile, Tenant tenant) {
		return new Specification<Profile>() {

			@Override
			public Predicate toPredicate(Root<Profile> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				
				if (filterProfile.getAvatar() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("avatar")), "%" + filterProfile.getAvatar().toUpperCase() + "%"));
				}
				if (filterProfile.getBirthDate() != null) {
					predicates.add(criteriaBuilder.equal(root.get("birthDate"), filterProfile.getBirthDate()));
				}				
				if (filterProfile.getName() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")), "%" + filterProfile.getName().toUpperCase() + "%"));
				}
				if (filterProfile.getCustomer() != null) {
					predicates.add(criteriaBuilder.equal(root.get("customer").get("id"), filterProfile.getCustomer()));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}
}
