package br.com.netflics.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import br.com.netflics.model.MaturityRating;
import br.com.netflics.model.filter.FilterMaturityRating;

import org.springframework.data.jpa.domain.Specification;

import br.com.netflics.core.model.Tenant;
@SuppressWarnings("serial")
public class MaturityRatingSpecificationHelper {

	public static Specification<MaturityRating> fromId(Integer id, Tenant tenant) {
		return new Specification<MaturityRating>() {
			@Override
			public Predicate toPredicate(Root<MaturityRating> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				predicates.add(criteriaBuilder.equal(root.get("id"), id));

				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}

	public static Specification<MaturityRating> filter(FilterMaturityRating filterMaturityRating, Tenant tenant) {
		return new Specification<MaturityRating>() {

			@Override
			public Predicate toPredicate(Root<MaturityRating> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				
				if (filterMaturityRating.getMinimalAge() != null) {
					predicates.add(criteriaBuilder.equal(root.get("minimalAge"), filterMaturityRating.getMinimalAge()));
				}				
				if (filterMaturityRating.getName() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")), "%" + filterMaturityRating.getName().toUpperCase() + "%"));
				}
				if (filterMaturityRating.getDescription() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("description")), "%" + filterMaturityRating.getDescription().toUpperCase() + "%"));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}
}
