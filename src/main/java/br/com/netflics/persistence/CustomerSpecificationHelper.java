package br.com.netflics.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import br.com.netflics.model.Customer;
import br.com.netflics.model.filter.FilterCustomer;

import org.springframework.data.jpa.domain.Specification;

import br.com.netflics.core.model.Tenant;
@SuppressWarnings("serial")
public class CustomerSpecificationHelper {

	public static Specification<Customer> fromId(Integer id, Tenant tenant) {
		return new Specification<Customer>() {
			@Override
			public Predicate toPredicate(Root<Customer> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				predicates.add(criteriaBuilder.equal(root.get("id"), id));

				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}

	public static Specification<Customer> filter(FilterCustomer filterCustomer, Tenant tenant) {
		return new Specification<Customer>() {

			@Override
			public Predicate toPredicate(Root<Customer> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				
				if (filterCustomer.getTelephone() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("telephone")), "%" + filterCustomer.getTelephone().toUpperCase() + "%"));
				}
				if (filterCustomer.getDiscount() != null) {
					predicates.add(criteriaBuilder.equal(root.get("discount"), filterCustomer.getDiscount()));
				}				
				if (filterCustomer.getDueDate() != null) {
					predicates.add(criteriaBuilder.equal(root.get("dueDate"), filterCustomer.getDueDate()));
				}				
				if (filterCustomer.getCpf() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("cpf")), "%" + filterCustomer.getCpf().toUpperCase() + "%"));
				}
				if (filterCustomer.getName() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")), "%" + filterCustomer.getName().toUpperCase() + "%"));
				}
				if (filterCustomer.getMonthlyFee() != null) {
					predicates.add(criteriaBuilder.equal(root.get("monthlyFee"), filterCustomer.getMonthlyFee()));
				}				
				if (filterCustomer.getObservation() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("observation")), "%" + filterCustomer.getObservation().toUpperCase() + "%"));
				}
				if (filterCustomer.getActive() != null) {
					predicates.add(criteriaBuilder.equal(root.get("active"), filterCustomer.getActive()));
				}				
				if (filterCustomer.getPicture() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("picture")), "%" + filterCustomer.getPicture().toUpperCase() + "%"));
				}
				if (filterCustomer.getCelphone() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("celphone")), "%" + filterCustomer.getCelphone().toUpperCase() + "%"));
				}
				if (filterCustomer.getRg() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("rg")), "%" + filterCustomer.getRg().toUpperCase() + "%"));
				}
				if (filterCustomer.getSubscriptionDatetime() != null) {
					predicates.add(criteriaBuilder.equal(root.get("subscriptionDatetime"), filterCustomer.getSubscriptionDatetime()));
				}				
				if (filterCustomer.getBirthDate() != null) {
					predicates.add(criteriaBuilder.equal(root.get("birthDate"), filterCustomer.getBirthDate()));
				}				
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}
}
