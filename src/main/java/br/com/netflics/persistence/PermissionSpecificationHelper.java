package br.com.netflics.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import br.com.netflics.model.Permission;
import br.com.netflics.model.filter.FilterPermission;

import org.springframework.data.jpa.domain.Specification;

import br.com.netflics.core.model.Tenant;
@SuppressWarnings("serial")
public class PermissionSpecificationHelper {

	public static Specification<Permission> fromId(Integer id, Tenant tenant) {
		return new Specification<Permission>() {
			@Override
			public Predicate toPredicate(Root<Permission> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				predicates.add(criteriaBuilder.equal(root.get("id"), id));

				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}

	public static Specification<Permission> filter(FilterPermission filterPermission, Tenant tenant) {
		return new Specification<Permission>() {

			@Override
			public Predicate toPredicate(Root<Permission> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(root.get("tenant"), tenant));
				
				if (filterPermission.getName() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")), "%" + filterPermission.getName().toUpperCase() + "%"));
				}
				if (filterPermission.getDescription() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("description")), "%" + filterPermission.getDescription().toUpperCase() + "%"));
				}
				if (filterPermission.getOperation() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("operation")), "%" + filterPermission.getOperation().toUpperCase() + "%"));
				}
				if (filterPermission.getTagReminder() != null) {
					predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("tagReminder")), "%" + filterPermission.getTagReminder().toUpperCase() + "%"));
				}
				if (filterPermission.getItem() != null) {
					predicates.add(criteriaBuilder.equal(root.get("item").get("id"), filterPermission.getItem()));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
	}
}
