package br.com.netflics.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("br.com.netflics")
@EntityScan({ "br.com.netflics.model", "br.com.netflics.core.model" })
@EnableJpaRepositories("br.com.netflics")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
} 