/* generated by JSetup v0.95 :  at 24/03/2019 17:51:19 */
define(function(require) {
	var util = require('utilities/utils');
	var JSetup = require('views/components/JSetup');	
	var PageProfileTemplate = require('text!views/profile/tpl/PageProfileTemplate.html');

	var Profile = require('models/Profile');

	var PageProfile = JSetup.View.extend({
		template : _.template(PageProfileTemplate),

		/** The declared form Regions. */
		regions : {
			dataTableProfileRegion : '.datatable-profile',
		},
		
		/** The form events you'd like to listen */
		events : {
			'click 	.reset-button' : 'resetProfile',			
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchProfile',
			'click  .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		/** All the important fields must be here. */		
		ui : {
			loadButton : '.loading-button',
			inputAvatar : '#inputAvatar',
			inputBirthDate : '#inputBirthDate',
			inputName : '#inputName',
			form : '#formProfileFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		/** First function called, like a constructor. */		
		initialize : function() {
			var that = this;
			this.profiles = new Profile.PageCollection();
			
			this.dataTableProfile = new JSetup.DataTable({
				columns : this.getColumns(),
				collection : this.profiles,
				onFetching : this.startFetch,
				onFetched : this.stopFetch,
				view : this
			});			
		},

		/** Called after DOM´s ready.*/		
		onShowView : function() {
			var that = this;
			this.ui.inputBirthDate.date();
		
			this.dataTableProfileRegion.show(this.dataTableProfile);
		
			this.dataTableProfile.recoveryLastQuery();
		},
		
		 searchProfile : function(){		                                                                   
		 	var that = this;                                                                                                   
		 	this.dataTableProfile.getFirstPage({                                                                             
		 		success : function(_coll, _resp, _opt) {                                                                       
		 			//console.info('Consulta para o grid profile');                                         
		 		},                                                                                                             
		 		error : function(_coll, _resp, _opt) {                                                                         
		 			//console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));  
		 		},                                                                                                             
		 		filterQueryParams : {                                                                                          
		     		birthDate : this.ui.inputBirthDate.escape(),                                   
		     		name : this.ui.inputName.escape(),                                   
		 		}                                                                                                              
		 	})                                                                                                                 
		 },																													   
		
		resetProfile : function(){
			this.ui.form.get(0).reset();
			this.profiles.reset();
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "birthDate",
				sortable : true,
				editable : false,
				label 	 : "Birth date",
				cell : JSetup.CustomStringCell
			}, 
			{
				name : "name",
				sortable : true,
				editable : false,
				label 	 : "Name",
				cell : JSetup.CustomStringCell
			}, 
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				editable : false,
				sortable : false,
				cell : JSetup.ActionCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edit_button',
				type : 'primary',
				icon : 'fa-pencil',
				customClass : 'auth[edit-profile,disable]',
				hint : 'Editar Profile',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				customClass : 'auth[delete-profile, disable]',
				icon : 'fa-trash',
				hint : 'Remover Profile',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new Profile.Model({id : model.id});
			
			util.confirm({
				title : "Importante",
				text : "Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?",
				onConfirm : function() {
					modelTipo.destroy({
						success : function() {
							that.profiles.remove(model);
							util.alert({title : "Concluido", text : "Registro removido com sucesso!"});
						},
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editProfile/" + model.get('id'));
		},

		
		// additional functions
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		treatKeypress : function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchProfile();
			}
		},
		startFetch : function() {
			util.loadButton(this.ui.loadButton)
		},

		stopFetch : function() {
			util.resetButton(this.ui.loadButton)
		}
	});

	return PageProfile;
});
