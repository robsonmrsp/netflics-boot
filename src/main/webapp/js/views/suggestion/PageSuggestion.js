/* generated by JSetup v0.95 :  at 24/03/2019 17:51:19 */
define(function(require) {
	var util = require('utilities/utils');
	var JSetup = require('views/components/JSetup');	
	var PageSuggestionTemplate = require('text!views/suggestion/tpl/PageSuggestionTemplate.html');

	var Suggestion = require('models/Suggestion');

	var PageSuggestion = JSetup.View.extend({
		template : _.template(PageSuggestionTemplate),

		/** The declared form Regions. */
		regions : {
			dataTableSuggestionRegion : '.datatable-suggestion',
		},
		
		/** The form events you'd like to listen */
		events : {
			'click 	.reset-button' : 'resetSuggestion',			
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchSuggestion',
			'click  .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		/** All the important fields must be here. */		
		ui : {
			loadButton : '.loading-button',
			inputExpirationDate : '#inputExpirationDate',
			form : '#formSuggestionFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		/** First function called, like a constructor. */		
		initialize : function() {
			var that = this;
			this.suggestions = new Suggestion.PageCollection();
			
			this.dataTableSuggestion = new JSetup.DataTable({
				columns : this.getColumns(),
				collection : this.suggestions,
				onFetching : this.startFetch,
				onFetched : this.stopFetch,
				view : this
			});			
		},

		/** Called after DOM´s ready.*/		
		onShowView : function() {
			var that = this;
			this.ui.inputExpirationDate.date();
		
			this.dataTableSuggestionRegion.show(this.dataTableSuggestion);
		
			this.dataTableSuggestion.recoveryLastQuery();
		},
		
		 searchSuggestion : function(){		                                                                   
		 	var that = this;                                                                                                   
		 	this.dataTableSuggestion.getFirstPage({                                                                             
		 		success : function(_coll, _resp, _opt) {                                                                       
		 			//console.info('Consulta para o grid suggestion');                                         
		 		},                                                                                                             
		 		error : function(_coll, _resp, _opt) {                                                                         
		 			//console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));  
		 		},                                                                                                             
		 		filterQueryParams : {                                                                                          
		     		expirationDate : this.ui.inputExpirationDate.escape(),                                   
		 		}                                                                                                              
		 	})                                                                                                                 
		 },																													   
		
		resetSuggestion : function(){
			this.ui.form.get(0).reset();
			this.suggestions.reset();
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "expirationDate",
				sortable : true,
				editable : false,
				label 	 : "Expiration date",
				cell : JSetup.CustomStringCell
			}, 
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				editable : false,
				sortable : false,
				cell : JSetup.ActionCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edit_button',
				type : 'primary',
				icon : 'fa-pencil',
				customClass : 'auth[edit-suggestion,disable]',
				hint : 'Editar Suggestion',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				customClass : 'auth[delete-suggestion, disable]',
				icon : 'fa-trash',
				hint : 'Remover Suggestion',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new Suggestion.Model({id : model.id});
			
			util.confirm({
				title : "Importante",
				text : "Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?",
				onConfirm : function() {
					modelTipo.destroy({
						success : function() {
							that.suggestions.remove(model);
							util.alert({title : "Concluido", text : "Registro removido com sucesso!"});
						},
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editSuggestion/" + model.get('id'));
		},

		
		// additional functions
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		treatKeypress : function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchSuggestion();
			}
		},
		startFetch : function() {
			util.loadButton(this.ui.loadButton)
		},

		stopFetch : function() {
			util.resetButton(this.ui.loadButton)
		}
	});

	return PageSuggestion;
});
