define(function(require) {
	var Router = require('Router');
	describe("Rotas", function() {

		beforeEach(function() {
			try {
				Backbone.history.stop();
			} catch (e) {
				console.error(e);
			}
		});
		
		afterEach(function() {
			// Reset URL
			var router = new Router();
			router.navigate("");
		});
		
				it("Rota de 'Movies'", function() {
			spyOn(Router.prototype, "movies")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/movies', true);
			expect(Router.prototype.movies).toHaveBeenCalled();
		});

		it("Rota de 'newMovie'", function() {
			spyOn(Router.prototype, "newMovie")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newMovie', true);
			expect(Router.prototype.newMovie).toHaveBeenCalled();
		});
		
		it("Rota de 'editMovie'", function() {
			spyOn(Router.prototype, "editMovie")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editMovie/1', true);
			expect(Router.prototype.editMovie).toHaveBeenCalled();
		});
		it("Rota de 'Actors'", function() {
			spyOn(Router.prototype, "actors")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/actors', true);
			expect(Router.prototype.actors).toHaveBeenCalled();
		});

		it("Rota de 'newActor'", function() {
			spyOn(Router.prototype, "newActor")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newActor', true);
			expect(Router.prototype.newActor).toHaveBeenCalled();
		});
		
		it("Rota de 'editActor'", function() {
			spyOn(Router.prototype, "editActor")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editActor/1', true);
			expect(Router.prototype.editActor).toHaveBeenCalled();
		});
		it("Rota de 'Genres'", function() {
			spyOn(Router.prototype, "genres")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/genres', true);
			expect(Router.prototype.genres).toHaveBeenCalled();
		});

		it("Rota de 'newGenre'", function() {
			spyOn(Router.prototype, "newGenre")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newGenre', true);
			expect(Router.prototype.newGenre).toHaveBeenCalled();
		});
		
		it("Rota de 'editGenre'", function() {
			spyOn(Router.prototype, "editGenre")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editGenre/1', true);
			expect(Router.prototype.editGenre).toHaveBeenCalled();
		});
		it("Rota de 'MaturityRatings'", function() {
			spyOn(Router.prototype, "maturityRatings")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/maturityRatings', true);
			expect(Router.prototype.maturityRatings).toHaveBeenCalled();
		});

		it("Rota de 'newMaturityRating'", function() {
			spyOn(Router.prototype, "newMaturityRating")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newMaturityRating', true);
			expect(Router.prototype.newMaturityRating).toHaveBeenCalled();
		});
		
		it("Rota de 'editMaturityRating'", function() {
			spyOn(Router.prototype, "editMaturityRating")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editMaturityRating/1', true);
			expect(Router.prototype.editMaturityRating).toHaveBeenCalled();
		});
		it("Rota de 'Suggestions'", function() {
			spyOn(Router.prototype, "suggestions")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/suggestions', true);
			expect(Router.prototype.suggestions).toHaveBeenCalled();
		});

		it("Rota de 'newSuggestion'", function() {
			spyOn(Router.prototype, "newSuggestion")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newSuggestion', true);
			expect(Router.prototype.newSuggestion).toHaveBeenCalled();
		});
		
		it("Rota de 'editSuggestion'", function() {
			spyOn(Router.prototype, "editSuggestion")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editSuggestion/1', true);
			expect(Router.prototype.editSuggestion).toHaveBeenCalled();
		});
		it("Rota de 'Profiles'", function() {
			spyOn(Router.prototype, "profiles")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/profiles', true);
			expect(Router.prototype.profiles).toHaveBeenCalled();
		});

		it("Rota de 'newProfile'", function() {
			spyOn(Router.prototype, "newProfile")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newProfile', true);
			expect(Router.prototype.newProfile).toHaveBeenCalled();
		});
		
		it("Rota de 'editProfile'", function() {
			spyOn(Router.prototype, "editProfile")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editProfile/1', true);
			expect(Router.prototype.editProfile).toHaveBeenCalled();
		});
		it("Rota de 'Customers'", function() {
			spyOn(Router.prototype, "customers")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/customers', true);
			expect(Router.prototype.customers).toHaveBeenCalled();
		});

		it("Rota de 'newCustomer'", function() {
			spyOn(Router.prototype, "newCustomer")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newCustomer', true);
			expect(Router.prototype.newCustomer).toHaveBeenCalled();
		});
		
		it("Rota de 'editCustomer'", function() {
			spyOn(Router.prototype, "editCustomer")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editCustomer/1', true);
			expect(Router.prototype.editCustomer).toHaveBeenCalled();
		});
		it("Rota de 'Users'", function() {
			spyOn(Router.prototype, "users")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/users', true);
			expect(Router.prototype.users).toHaveBeenCalled();
		});

		it("Rota de 'newUser'", function() {
			spyOn(Router.prototype, "newUser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newUser', true);
			expect(Router.prototype.newUser).toHaveBeenCalled();
		});
		
		it("Rota de 'editUser'", function() {
			spyOn(Router.prototype, "editUser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editUser/1', true);
			expect(Router.prototype.editUser).toHaveBeenCalled();
		});
		it("Rota de 'Roles'", function() {
			spyOn(Router.prototype, "roles")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/roles', true);
			expect(Router.prototype.roles).toHaveBeenCalled();
		});

		it("Rota de 'newRole'", function() {
			spyOn(Router.prototype, "newRole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newRole', true);
			expect(Router.prototype.newRole).toHaveBeenCalled();
		});
		
		it("Rota de 'editRole'", function() {
			spyOn(Router.prototype, "editRole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editRole/1', true);
			expect(Router.prototype.editRole).toHaveBeenCalled();
		});
		it("Rota de 'Permissions'", function() {
			spyOn(Router.prototype, "permissions")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/permissions', true);
			expect(Router.prototype.permissions).toHaveBeenCalled();
		});

		it("Rota de 'newPermission'", function() {
			spyOn(Router.prototype, "newPermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newPermission', true);
			expect(Router.prototype.newPermission).toHaveBeenCalled();
		});
		
		it("Rota de 'editPermission'", function() {
			spyOn(Router.prototype, "editPermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editPermission/1', true);
			expect(Router.prototype.editPermission).toHaveBeenCalled();
		});
		it("Rota de 'Groups'", function() {
			spyOn(Router.prototype, "groups")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/groups', true);
			expect(Router.prototype.groups).toHaveBeenCalled();
		});

		it("Rota de 'newGroup'", function() {
			spyOn(Router.prototype, "newGroup")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newGroup', true);
			expect(Router.prototype.newGroup).toHaveBeenCalled();
		});
		
		it("Rota de 'editGroup'", function() {
			spyOn(Router.prototype, "editGroup")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editGroup/1', true);
			expect(Router.prototype.editGroup).toHaveBeenCalled();
		});
		it("Rota de 'Items'", function() {
			spyOn(Router.prototype, "items")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/items', true);
			expect(Router.prototype.items).toHaveBeenCalled();
		});

		it("Rota de 'newItem'", function() {
			spyOn(Router.prototype, "newItem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newItem', true);
			expect(Router.prototype.newItem).toHaveBeenCalled();
		});
		
		it("Rota de 'editItem'", function() {
			spyOn(Router.prototype, "editItem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editItem/1', true);
			expect(Router.prototype.editItem).toHaveBeenCalled();
		});
	});
})
