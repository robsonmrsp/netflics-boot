({

	baseUrl : "../../src/main/webapp/js",
	name : "LoginMain",
	optimizeCss : "standard.keepLines",
	inlineText : true,
	mainConfigFile : "../../src/main/webapp/js/LoginMain.js",
	removeCombined : true,
	include : [ "../../webapp/vendor/require/require" ],
	exclude : [],
	optimize : "uglify2",
	out : "../../src/main/webapp/javascript/main-login-built.js",
})
