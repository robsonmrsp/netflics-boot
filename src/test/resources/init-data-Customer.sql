 ALTER SEQUENCE hibernate_sequence RESTART WITH 10001;

 TRUNCATE TABLE tenant CASCADE;
 TRUNCATE TABLE app_user CASCADE;
 TRUNCATE TABLE role CASCADE;
 TRUNCATE TABLE user_role CASCADE;

 INSERT INTO tenant (id, cnpj, corporate_name,  phone_number, logo, name) values (101,'','JSetup Developer', '997608620','','JSetup Developer');

 INSERT INTO role( id, id_tenant, authority, description)  VALUES (101, 101, 'ROLE_USER', 'Usuário do sistema');
 INSERT INTO app_user( id, id_tenant, enable, image, name, password, username, email) VALUES (101, 101, true, '', 'Usuário JSetup Comum', '$2a$10$teJrCEnsxNT49ZpXU7n22O27aCGbVYYe/RG6/XxdWPJbOLZubLIi2', 'jsetup', 'contato@jsetup.com');
 INSERT INTO user_role(id_role, id_user) values (101, 101);

TRUNCATE TABLE CUSTOMER CASCADE;
INSERT INTO CUSTOMER 	( id, id_tenant
			,TELEPHONE
			,CPF
			,NAME
			,OBSERVATION
			,PICTURE
			,CELPHONE
			,RG
			)values(1, 101
			, 'telephone customer1'
			, 'cpf customer1'
			, 'name customer1'
			, 'observation customer1'
			, 'picture customer1'
			, 'celphone customer1'
			, 'rg customer1'
			);
INSERT INTO CUSTOMER 	( id, id_tenant
			,TELEPHONE
			,CPF
			,NAME
			,OBSERVATION
			,PICTURE
			,CELPHONE
			,RG
			)values(2, 101
			, 'telephone customer2'
			, 'cpf customer2'
			, 'name customer2'
			, 'observation customer2'
			, 'picture customer2'
			, 'celphone customer2'
			, 'rg customer2'
			);
INSERT INTO CUSTOMER 	( id, id_tenant
			,TELEPHONE
			,CPF
			,NAME
			,OBSERVATION
			,PICTURE
			,CELPHONE
			,RG
			)values(3, 101
			, 'telephone customer3'
			, 'cpf customer3'
			, 'name customer3'
			, 'observation customer3'
			, 'picture customer3'
			, 'celphone customer3'
			, 'rg customer3'
			);
INSERT INTO CUSTOMER 	( id, id_tenant
			,TELEPHONE
			,CPF
			,NAME
			,OBSERVATION
			,PICTURE
			,CELPHONE
			,RG
			)values(4, 101
			, 'telephone customer4'
			, 'cpf customer4'
			, 'name customer4'
			, 'observation customer4'
			, 'picture customer4'
			, 'celphone customer4'
			, 'rg customer4'
			);
INSERT INTO CUSTOMER 	( id, id_tenant
			,TELEPHONE
			,CPF
			,NAME
			,OBSERVATION
			,PICTURE
			,CELPHONE
			,RG
			)values(5, 101
			, 'telephone customer5'
			, 'cpf customer5'
			, 'name customer5'
			, 'observation customer5'
			, 'picture customer5'
			, 'celphone customer5'
			, 'rg customer5'
			);
