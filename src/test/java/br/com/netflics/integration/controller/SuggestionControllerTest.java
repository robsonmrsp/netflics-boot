/** generated: 24/03/2019 17:51:19 **/
package br.com.netflics.integration.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.netflics.core.persistence.pagination.Pager;
import br.com.netflics.model.Suggestion;
import br.com.netflics.fixture.FixtureUtils;
import br.com.six2six.fixturefactory.Fixture;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Sql("classpath:init-data-Suggestion.sql")
public class SuggestionControllerTest {

	@Autowired
	TestRestTemplate testRestTemplate;

	private static final String URL = "/rs/crud/suggestions";

	@BeforeClass
	public static void setUp() {
		FixtureUtils.init();
	}

	@Before
	public void before() {
	}

	@Test
	public void testAddSuggestion() throws Exception {

		Suggestion suggestion = Fixture.from(Suggestion.class).gimme("novo");
		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Suggestion> responseEntity = withBasicAuth.postForEntity(URL, suggestion, Suggestion.class);

		HttpStatus status = responseEntity.getStatusCode();
		Suggestion resultSuggestion = responseEntity.getBody();

		assertEquals("Incorrect Response Status: ", HttpStatus.CREATED, status);
		assertNotNull("A not null gender should be returned: ", resultSuggestion);
		assertNotNull("A not null gender identifier should be returned:", resultSuggestion.getId());
	}

	@Test
	public void testGetSuggestion() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Suggestion> responseEntity = withBasicAuth.getForEntity(URL + "/{id}", Suggestion.class, new Integer(1));

		HttpStatus status = responseEntity.getStatusCode();
		Suggestion resultSuggestion = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultSuggestion);
		assertEquals("A id gender == 1 must be returned: ", resultSuggestion.getId(), new Integer(1));
	}

	@Test
	public void testGetPagerSuggestion() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Pager> responseEntity = withBasicAuth.getForEntity(URL, Pager.class);

		HttpStatus status = responseEntity.getStatusCode();
		Pager<Suggestion> resultPagerSuggestion = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultPagerSuggestion);
	}

	@Test
	public void testGetSuggestionNotExist() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Suggestion> responseEntity = withBasicAuth.getForEntity(URL + "/{id}", Suggestion.class, new Long(100));

		HttpStatus status = responseEntity.getStatusCode();
		Suggestion resultSuggestion = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.NOT_FOUND, status);
		assertNull(resultSuggestion);
	}

	@Test
	public void testGetSuggestionByParameter() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Pager<Suggestion>> responseEntity = withBasicAuth.exchange(URL + "?nO_NAME={nO_NAME}", HttpMethod.GET, null, new ParameterizedTypeReference<Pager<Suggestion>>() {}, "nO_NAME suggestion1");
		Pager<Suggestion> suggestions = responseEntity.getBody();
		HttpStatus status = responseEntity.getStatusCode();

		suggestions.getItems().forEach(new Consumer<Suggestion>() {
			@Override
			public void accept(Suggestion suggestion) {
			}
		});

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertTrue("A Array of Suggestion should be returned ", suggestions.getItems().size() > 0);
	}
	
	@Test
	public void testUpdateSuggestion() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		Suggestion suggestion = Fixture.from(Suggestion.class).gimme("novo");
		suggestion.setId(1);

		HttpEntity<Suggestion> requestEntity = new HttpEntity<Suggestion>(suggestion);

		ResponseEntity<Suggestion> responseEntity = withBasicAuth.exchange(URL + "/{id}", HttpMethod.PUT, requestEntity, Suggestion.class, new Integer(1));

		HttpStatus status = responseEntity.getStatusCode();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);

	}

	@Test
	public void testDeleteSuggestion() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Boolean> responseEntity = withBasicAuth.exchange(URL + "/{id}", HttpMethod.DELETE, null, Boolean.class, new Integer(1));

		HttpStatus status = responseEntity.getStatusCode();

		ResponseEntity<Suggestion> responseTesteDelete = withBasicAuth.getForEntity(URL + "/{id}", Suggestion.class, new Integer(1));

		HttpStatus responseTesteDeleteStatus = responseTesteDelete.getStatusCode();
		Suggestion resultSuggestion = responseTesteDelete.getBody();

		assertEquals("Incorrect Response Status after delete the suggestion id = 1", HttpStatus.NOT_FOUND, responseTesteDeleteStatus);
		assertNull(resultSuggestion);

		assertEquals("Incorrect Response Status", HttpStatus.NO_CONTENT, status);

	}
}
//generated by JSetup v0.95 :  at 24/03/2019 17:51:19