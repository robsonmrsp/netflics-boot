/** generated: 24/03/2019 17:51:19 **/
package br.com.netflics.integration.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.netflics.core.persistence.pagination.Pager;
import br.com.netflics.model.Profile;
import br.com.netflics.fixture.FixtureUtils;
import br.com.six2six.fixturefactory.Fixture;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Sql("classpath:init-data-Profile.sql")
public class ProfileControllerTest {

	@Autowired
	TestRestTemplate testRestTemplate;

	private static final String URL = "/rs/crud/profiles";

	@BeforeClass
	public static void setUp() {
		FixtureUtils.init();
	}

	@Before
	public void before() {
	}

	@Test
	public void testAddProfile() throws Exception {

		Profile profile = Fixture.from(Profile.class).gimme("novo");
		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Profile> responseEntity = withBasicAuth.postForEntity(URL, profile, Profile.class);

		HttpStatus status = responseEntity.getStatusCode();
		Profile resultProfile = responseEntity.getBody();

		assertEquals("Incorrect Response Status: ", HttpStatus.CREATED, status);
		assertNotNull("A not null gender should be returned: ", resultProfile);
		assertNotNull("A not null gender identifier should be returned:", resultProfile.getId());
	}

	@Test
	public void testGetProfile() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Profile> responseEntity = withBasicAuth.getForEntity(URL + "/{id}", Profile.class, new Integer(1));

		HttpStatus status = responseEntity.getStatusCode();
		Profile resultProfile = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultProfile);
		assertEquals("A id gender == 1 must be returned: ", resultProfile.getId(), new Integer(1));
	}

	@Test
	public void testGetPagerProfile() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Pager> responseEntity = withBasicAuth.getForEntity(URL, Pager.class);

		HttpStatus status = responseEntity.getStatusCode();
		Pager<Profile> resultPagerProfile = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultPagerProfile);
	}

	@Test
	public void testGetProfileNotExist() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Profile> responseEntity = withBasicAuth.getForEntity(URL + "/{id}", Profile.class, new Long(100));

		HttpStatus status = responseEntity.getStatusCode();
		Profile resultProfile = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.NOT_FOUND, status);
		assertNull(resultProfile);
	}

	@Test
	public void testGetProfileByParameter() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Pager<Profile>> responseEntity = withBasicAuth.exchange(URL + "?avatar={avatar}", HttpMethod.GET, null, new ParameterizedTypeReference<Pager<Profile>>() {}, "avatar profile1");
		Pager<Profile> profiles = responseEntity.getBody();
		HttpStatus status = responseEntity.getStatusCode();

		profiles.getItems().forEach(new Consumer<Profile>() {
			@Override
			public void accept(Profile profile) {
				assertEquals("A not null Profile should be returned white the 'name' = 'avatar profile1'", profile.getAvatar(), "avatar profile1");
			}
		});

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertTrue("A Array of Profile should be returned ", profiles.getItems().size() > 0);
	}
	
	@Test
	public void testUpdateProfile() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		Profile profile = Fixture.from(Profile.class).gimme("novo");
		profile.setId(1);

		HttpEntity<Profile> requestEntity = new HttpEntity<Profile>(profile);

		ResponseEntity<Profile> responseEntity = withBasicAuth.exchange(URL + "/{id}", HttpMethod.PUT, requestEntity, Profile.class, new Integer(1));

		HttpStatus status = responseEntity.getStatusCode();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);

	}

	@Test
	public void testDeleteProfile() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Boolean> responseEntity = withBasicAuth.exchange(URL + "/{id}", HttpMethod.DELETE, null, Boolean.class, new Integer(1));

		HttpStatus status = responseEntity.getStatusCode();

		ResponseEntity<Profile> responseTesteDelete = withBasicAuth.getForEntity(URL + "/{id}", Profile.class, new Integer(1));

		HttpStatus responseTesteDeleteStatus = responseTesteDelete.getStatusCode();
		Profile resultProfile = responseTesteDelete.getBody();

		assertEquals("Incorrect Response Status after delete the profile id = 1", HttpStatus.NOT_FOUND, responseTesteDeleteStatus);
		assertNull(resultProfile);

		assertEquals("Incorrect Response Status", HttpStatus.NO_CONTENT, status);

	}
}
//generated by JSetup v0.95 :  at 24/03/2019 17:51:19