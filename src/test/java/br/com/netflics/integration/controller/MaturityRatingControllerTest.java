/** generated: 24/03/2019 17:51:19 **/
package br.com.netflics.integration.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.netflics.core.persistence.pagination.Pager;
import br.com.netflics.model.MaturityRating;
import br.com.netflics.fixture.FixtureUtils;
import br.com.six2six.fixturefactory.Fixture;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Sql("classpath:init-data-MaturityRating.sql")
public class MaturityRatingControllerTest {

	@Autowired
	TestRestTemplate testRestTemplate;

	private static final String URL = "/rs/crud/maturityRatings";

	@BeforeClass
	public static void setUp() {
		FixtureUtils.init();
	}

	@Before
	public void before() {
	}

	@Test
	public void testAddMaturityRating() throws Exception {

		MaturityRating maturityRating = Fixture.from(MaturityRating.class).gimme("novo");
		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<MaturityRating> responseEntity = withBasicAuth.postForEntity(URL, maturityRating, MaturityRating.class);

		HttpStatus status = responseEntity.getStatusCode();
		MaturityRating resultMaturityRating = responseEntity.getBody();

		assertEquals("Incorrect Response Status: ", HttpStatus.CREATED, status);
		assertNotNull("A not null gender should be returned: ", resultMaturityRating);
		assertNotNull("A not null gender identifier should be returned:", resultMaturityRating.getId());
	}

	@Test
	public void testGetMaturityRating() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<MaturityRating> responseEntity = withBasicAuth.getForEntity(URL + "/{id}", MaturityRating.class, new Integer(1));

		HttpStatus status = responseEntity.getStatusCode();
		MaturityRating resultMaturityRating = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultMaturityRating);
		assertEquals("A id gender == 1 must be returned: ", resultMaturityRating.getId(), new Integer(1));
	}

	@Test
	public void testGetPagerMaturityRating() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Pager> responseEntity = withBasicAuth.getForEntity(URL, Pager.class);

		HttpStatus status = responseEntity.getStatusCode();
		Pager<MaturityRating> resultPagerMaturityRating = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultPagerMaturityRating);
	}

	@Test
	public void testGetMaturityRatingNotExist() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<MaturityRating> responseEntity = withBasicAuth.getForEntity(URL + "/{id}", MaturityRating.class, new Long(100));

		HttpStatus status = responseEntity.getStatusCode();
		MaturityRating resultMaturityRating = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.NOT_FOUND, status);
		assertNull(resultMaturityRating);
	}

	@Test
	public void testGetMaturityRatingByParameter() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Pager<MaturityRating>> responseEntity = withBasicAuth.exchange(URL + "?name={name}", HttpMethod.GET, null, new ParameterizedTypeReference<Pager<MaturityRating>>() {}, "name maturityRating1");
		Pager<MaturityRating> maturityRatings = responseEntity.getBody();
		HttpStatus status = responseEntity.getStatusCode();

		maturityRatings.getItems().forEach(new Consumer<MaturityRating>() {
			@Override
			public void accept(MaturityRating maturityRating) {
				assertEquals("A not null MaturityRating should be returned white the 'name' = 'name maturityRating1'", maturityRating.getName(), "name maturityRating1");
			}
		});

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertTrue("A Array of MaturityRating should be returned ", maturityRatings.getItems().size() > 0);
	}
	
	@Test
	public void testUpdateMaturityRating() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		MaturityRating maturityRating = Fixture.from(MaturityRating.class).gimme("novo");
		maturityRating.setId(1);

		HttpEntity<MaturityRating> requestEntity = new HttpEntity<MaturityRating>(maturityRating);

		ResponseEntity<MaturityRating> responseEntity = withBasicAuth.exchange(URL + "/{id}", HttpMethod.PUT, requestEntity, MaturityRating.class, new Integer(1));

		HttpStatus status = responseEntity.getStatusCode();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);

	}

	@Test
	public void testDeleteMaturityRating() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Boolean> responseEntity = withBasicAuth.exchange(URL + "/{id}", HttpMethod.DELETE, null, Boolean.class, new Integer(1));

		HttpStatus status = responseEntity.getStatusCode();

		ResponseEntity<MaturityRating> responseTesteDelete = withBasicAuth.getForEntity(URL + "/{id}", MaturityRating.class, new Integer(1));

		HttpStatus responseTesteDeleteStatus = responseTesteDelete.getStatusCode();
		MaturityRating resultMaturityRating = responseTesteDelete.getBody();

		assertEquals("Incorrect Response Status after delete the maturityRating id = 1", HttpStatus.NOT_FOUND, responseTesteDeleteStatus);
		assertNull(resultMaturityRating);

		assertEquals("Incorrect Response Status", HttpStatus.NO_CONTENT, status);

	}
}
//generated by JSetup v0.95 :  at 24/03/2019 17:51:19