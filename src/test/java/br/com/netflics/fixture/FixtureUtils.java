package br.com.netflics.fixture;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;

import br.com.netflics.model.Movie;
import br.com.netflics.model.Actor;
import br.com.netflics.model.Genre;
import br.com.netflics.model.MaturityRating;
import br.com.netflics.model.Suggestion;
import br.com.netflics.model.Profile;
import br.com.netflics.model.Customer;
import br.com.netflics.model.User;
import br.com.netflics.model.Role;
import br.com.netflics.model.Permission;
import br.com.netflics.model.Group;
import br.com.netflics.model.Item;
public class FixtureUtils {

	public static void init() {
		Fixture.of(Movie.class).addTemplate("novo", new Rule() {
			{
				add("title", regex("[a-z ]{5,15}"));
				add("poster", regex("[a-z ]{5,15}"));
				add("sinopse", regex("[a-z ]{5,15}"));
				add("director", regex("[a-z ]{5,15}"));
			}
		});
		Fixture.of(Actor.class).addTemplate("novo", new Rule() {
			{
				add("biography", regex("[a-z ]{5,15}"));
				add("name", regex("[a-z ]{5,15}"));
				add("picture", regex("[a-z ]{5,15}"));
			}
		});
		Fixture.of(Genre.class).addTemplate("novo", new Rule() {
			{
				add("name", regex("[a-z ]{5,15}"));
				add("description", regex("[a-z ]{5,15}"));
			}
		});
		Fixture.of(MaturityRating.class).addTemplate("novo", new Rule() {
			{
				add("name", regex("[a-z ]{5,15}"));
				add("description", regex("[a-z ]{5,15}"));
			}
		});
		Fixture.of(Suggestion.class).addTemplate("novo", new Rule() {
			{
			}
		});
		Fixture.of(Profile.class).addTemplate("novo", new Rule() {
			{
				add("avatar", regex("[a-z ]{5,15}"));
				add("name", regex("[a-z ]{5,15}"));
			}
		});
		Fixture.of(Customer.class).addTemplate("novo", new Rule() {
			{
				add("telephone", regex("[a-z ]{5,15}"));
				add("cpf", regex("[a-z ]{5,15}"));
				add("name", regex("[a-z ]{5,15}"));
				add("observation", regex("[a-z ]{5,15}"));
				add("picture", regex("[a-z ]{5,15}"));
				add("celphone", regex("[a-z ]{5,15}"));
				add("rg", regex("[a-z ]{5,15}"));
			}
		});
		Fixture.of(User.class).addTemplate("novo", new Rule() {
			{
				add("name", regex("[a-z ]{5,15}"));
				add("username", regex("[a-z ]{5,15}"));
				add("email", regex("[a-z ]{5,15}"));
				add("password", regex("[a-z ]{5,15}"));
				add("image", regex("[a-z ]{5,15}"));
			}
		});
		Fixture.of(Role.class).addTemplate("novo", new Rule() {
			{
				add("authority", regex("[a-z ]{5,15}"));
				add("description", regex("[a-z ]{5,15}"));
			}
		});
		Fixture.of(Permission.class).addTemplate("novo", new Rule() {
			{
				add("name", regex("[a-z ]{5,15}"));
				add("description", regex("[a-z ]{5,15}"));
				add("operation", regex("[a-z ]{5,15}"));
				add("tagReminder", regex("[a-z ]{5,15}"));
			}
		});
		Fixture.of(Group.class).addTemplate("novo", new Rule() {
			{
				add("name", regex("[a-z ]{5,15}"));
				add("description", regex("[a-z ]{5,15}"));
			}
		});
		Fixture.of(Item.class).addTemplate("novo", new Rule() {
			{
				add("name", regex("[a-z ]{5,15}"));
				add("itemType", regex("[a-z ]{5,15}"));
				add("identifier", regex("[a-z ]{5,15}"));
				add("description", regex("[a-z ]{5,15}"));
			}
		});

	}
}